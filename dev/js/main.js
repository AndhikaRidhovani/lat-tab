/*! [PROJECT_NAME] | Suitmedia */

;(function ( window, document, undefined ) {

    var path = {
        css: myPrefix + 'assets/css/',
        js : myPrefix + 'assets/js/vendor/'
    };

    var assets = {
        _jquery_cdn     : 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js',
        _jquery_local   : path.js + 'jquery.min.js',
        _fastclick      : path.js + 'fastclick.min.js'
    };

    var Site = {

        init: function () {
            Site.fastClick();
            Site.enableActiveStateMobile();
            Site.WPViewportFix();
            Site.tabButton();
            Site.validate();

            window.Site = Site;
        },

        fastClick: function () {
            Modernizr.load({
                load    : assets._fastclick,
                complete: function () {
                    FastClick.attach(document.body);
                }
            });
        },

        enableActiveStateMobile: function () {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', function () {}, true);
            }
        },

        WPViewportFix: function () {
            if ( navigator.userAgent.match(/IEMobile\/10\.0/) ) {
                var style   = document.createElement("style"),
                    fix     = document.createTextNode("@-ms-viewport{width:auto!important}");

                style.appendChild(fix);
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        },

        tabButton: function () { 
            $('.tab-nav').click(function(event) {
                var tab_id = $(this).attr('href');
                $('.tab-nav').removeClass('is-active');
                $('.tab-panel').removeClass('is-active');

                $(this).addClass('is-active');
                $(tab_id).addClass('is-active'); 
            });
        },
          tabButton: function () { 
            var tabnav = $('.tab-nav');
            tabnav.click(function(event) {
                event.preventDefault;
            });

            var isActive = ('is-active');

            $('.tab-nav').click(function(event) {
                var tab_id = $(this).attr('href');
                tabnav.removeClass(isActive);
                $('.tab-panel').removeClass(isActive);

                $(this).addClass(isActive);
                $(tab_id).addClass(isActive); 
            });
        },

        validate: function () {
            $('form').submit(function(event) {
                var empty = 'empty';
                var complete = 'complete';
                var requireInput = $(event.target).find('[required]');
                var valid = true;

                requireInput.each(function(index, element) {
                    if ($(element).val() === '' ) {
                        valid = false ;
                        $(element).next('p').remove();
                        $(element).removeClass(complete);
                        $(element).addClass(empty);
                        $(element).after('<p>The box must be filled</p>');
                        event.preventDefault();
                    } else {
                        $(element).removeClass(empty);
                        $(element).addClass(complete);
                        $(element).next('p').remove();
                    }
                });
                if(valid === false )  {
                    var emptyInput = $(event.target).find('.' + empty)
                    emptyInput.first().focus();
                }
            });
        }
    };
    
    var checkJquery = function () {
        Modernizr.load([
            {
                test    : window.jQuery,
                nope    : assets._jquery_local,
                complete: Site.init
            }
        ]);
    };

    Modernizr.load({
        load    : assets._jquery_cdn,
        complete: checkJquery
    });

})( window, document );
